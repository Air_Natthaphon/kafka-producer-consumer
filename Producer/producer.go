package main

import (
	"fmt"
	"time"

	"strconv"

	"github.com/Shopify/sarama"
)

func main() {

	config := sarama.NewConfig()
	config.Producer.Return.Successes = true
	config.Producer.RequiredAcks = sarama.WaitForAll
	config.Producer.Retry.Max = 3

	brokers := []string{"localhost:9092"}
	producer, err := sarama.NewAsyncProducer(brokers, config)
	i := 0
	if err != nil {
		panic("Failed to create" + err.Error())
	}

	defer func() {
		if err := producer.Close(); err != nil {
			panic("Failed to close" + err.Error())
		}
	}()

	for {
		message := sarama.ProducerMessage{
			Topic: "test",
			Value: sarama.StringEncoder(strconv.Itoa(i)),
		}
		select {
		case producer.Input() <- &message:
			// fmt.Println(" producer received" , i)
			i++
			time.Sleep(1000 * time.Millisecond)
		case success := <-producer.Successes():
			fmt.Printf("Produce Topic: %s Partition: %v Offset: %v Message: %s\n", success.Topic, success.Partition, success.Offset, success.Value)
		case err := <-producer.Errors():
			fmt.Println("Failed to produce message:", err)
		}
	}
}
