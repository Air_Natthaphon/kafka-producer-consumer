package main

import (
	"fmt"
	"time"

	"github.com/Shopify/sarama"
)

func main() {

	config := sarama.NewConfig()
	config.Consumer.Offsets.Initial = sarama.OffsetOldest
	brokers := []string{"localhost:9092"}
	topic := "test"

	consumer, err := sarama.NewConsumer(brokers, config)
	if err != nil {
		panic("Error creating consumer " + err.Error())
	}

	defer func() {
		if err := consumer.Close(); err != nil {
			panic("Error closing consumer " + err.Error())
		}
	}()

	partitions, err := consumer.Partitions(topic)
	if err != nil {
		fmt.Println("Error getting partitions: " + err.Error())
		return
	}

	for partition := range partitions {
		consume, err := consumer.ConsumePartition(topic, int32(partition), sarama.OffsetOldest)
		if err != nil {
			fmt.Printf(" Error consuming partition %v: %s \n", partition, err)
			return
		}

		go func(consume sarama.PartitionConsumer) {
			for msg := range consume.Messages() {
				fmt.Printf("Consume Topic: %s Partition: %v  Offset: %v Message %s \n", msg.Topic, msg.Partition, msg.Offset, string(msg.Value))
			}

		}(consume)
	}
	time.Sleep(time.Hour)

}
